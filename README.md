<p align="center">
	<a href="https://gitee.com/wdyun/dynamic-datasource-spring-boot-starter">
	<h1 align="center">Dynamic Datasource</h1></a>
</p>
<p align="center">
	<strong>🍑 A springboot tool that provides dynamic data sources</strong>
</p>
<p align="center">
	👉 <a href="https://gitee.com/wdyun/dynamic-datasource-spring-boot-starter ">https://gitee.com/wdyun/dynamic-datasource-spring-boot-starter </a> 👈
</p>
<p align="center">
	👉 <a href="https://search.maven.org/artifact/com.enbatis/dynamic-datasource-spring-boot-starter">dynamic-datasource-spring-boot-starter</a> 👈
</p>

<p align="center">
	<a target="_blank" href="https://search.maven.org/artifact/com.enbatis/dynamic-datasource-spring-boot-starter">
		<img alt="Maven Central" src="https://maven-badges.herokuapp.com/maven-central/com.enbatis/dynamic-datasource-spring-boot-starter/badge.svg">
	</a>
	<a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0">
		<img src="https://img.shields.io/badge/license-Apache%202.0-green" />
	</a>
	<a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-1.8%2B-red" />
	</a>
	<a href="">
		<img src="https://gitee.com/wdyun/dynamic-datasource-spring-boot-starter/badge/star.svg?theme=dark"/>
	</a>
</p>
-------------------------------------------------------------------------------

[**🌎English Documentation**](README.en.md)

-------------------------------------------------------------------------------

## 📚简介
dynamic-datasource-spring-boot-starter是基于springboot开发的一款多数据源动态切换的开发工具。

只需在springboot项目中导入依赖 即可方便快捷的开发多数据源项目，并且支持多数据源的分布式事务。 

为您的多数据源项目开发保驾护航。


## 🍺目的

 专门为springboot项目提供动态数据源支持的开源java项目
 旨在提供多数据源的一键式支持。

## 🐮特性

- 多数据源配置简单，只需在配置文件中配置附加数据源即可。
- 支持多种数据库类型（mysql，postgresql，oracle）。
- 支持使用注解动态切换数据源
- 支持从 Mapper接口层切换多数据源，方便快捷，彻底的解决必须从 Controller 里切换数据源的麻烦。
- 提供mybatis环境下的纯读写分离方案。
- 提供基于jta atomikos的分布式事务。
- 支持分布式事务与普通事务的配置切换。
- 普通事务支持多种数据源（druid，c3po，dbcp）。


## 📦安装

Maven  

在项目的pom.xml的dependencies中加入动态数据源的依赖    

最新版本：<img alt="Maven Central" src="https://maven-badges.herokuapp.com/maven-central/com.enbatis/dynamic-datasource-spring-boot-starter/badge.svg">

```xml
<dependency>
  <groupId>com.enbatis</groupId>
  <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
  <version>最新版本</version>
</dependency>
```
在yml文件里面添加多数据源的配置

```yml
spring:
  datasource:
    dynamic:
      name: master,slave
      default: master
      master:
        type: com.alibaba.druid.pool.xa.DruidXADataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        username: xxx
        password: xxxxxxx
        url: jdbc:mysql://xxxxx:3306/test01?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
        initialSize: 5
        minIdle: 5
        maxActive: 20
      slave:
        type: com.alibaba.druid.pool.xa.DruidXADataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        username: xxx
        password: xxxxxxx
        url: jdbc:mysql://xxxxx:3306/test02?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
        initialSize: 5
        minIdle: 5
        maxActive: 20
```
在mapper接口添加数据源的配置(默认数据源可不添加)
（master为默认的数据源）
```java

@MybatisMapper(name = "master") 
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<SysUser> list1();
}

@MybatisMapper(name = "slave")
public interface SysUserMapper2 extends BaseMapper<SysUser> {

}
```
使用分布式事务示例如下:  
1.在配置文件中加入jta分布式事物的支持，代码如下
```
jta:
  enabled: true
  timeout: 180000  
```
2.在需要事务的方法上加入DynamicTx注解

```java
   
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper,SysUser > implements SysUserService {

    @DynamicTx
    @Override
    public boolean tx() {
            SysUser sysUser = new SysUser();
            sysUser.setId(IdWorker.getId());
            sysUser.setSex("男");
            sysUser.setUsername("sysUserMapper1");
            baseMapper.addEntity(sysUser);

            System.out.println(10/0);

            SysUser sysUser2 = new SysUser();
            sysUser2.setId(IdWorker.getId());
            sysUser2.setSex("男");
            sysUser2.setUsername("sysUserMapper2");
            sysUserMapper2.addEntity(sysUser2);
            return false;
    }

}

```
## 🚽示例项目

[https://gitee.com/wdyun/hrm](https://gitee.com/wdyun/hrm) 

使用方法：  
 1.导入项目  
 2.将配置文件数据库地址修改为自己的地址  
 ```
 spring:
   datasource:
     dynamic:
       name: master,slave
       default: master
       master:
         type: com.alibaba.druid.pool.DruidDataSource
         url: jdbc:mysql://127.0.0.1:3306/test01?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
         driver-class-name: com.mysql.cj.jdbc.Driver
         username: root
         password: 111111
         initialSize: 5
         minIdle: 5
         maxActive: 20
       slave:
         type: com.alibaba.druid.pool.DruidDataSource
         url: jdbc:mysql://127.0.0.1:3306/test02?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
         driver-class-name: com.mysql.cj.jdbc.Driver
         username: root
         password: 111111
         initialSize: 5
         minIdle: 5
         maxActive: 20
 ```
 3.新建数据库test01和test02，在两个库里面分别创建表sys_user    
 ```
 CREATE TABLE sys_user (
       id bigint NOT NULL COMMENT '主键 ID', 
       name varchar(30) COMMENT '姓名', 
       age int COMMENT '年龄', 
       phone varchar(11) COMMENT '电话',
       PRIMARY KEY (id) 
  )
 ```
 4.数据库需要授权 "XA_RECOVER_ADMIN" 权限，具体方式如下   
  ```
  grant XA_RECOVER_ADMIN  on *.* to 'username'@'%' with grant option;  
  FLUSH PRIVILEGES;
  ``` 
 5.启动项目（启动类为：HrmApplication）    
 6.访问接口tx（加了jta分布式注解，commit）,tx2（加了jta分布式注解，rollback）, tx3（未加注解）  
 
 http://localhost:9685/v1/sys_user/tx  
 http://localhost:9685/v1/sys_user/tx2  
 http://localhost:9685/v1/sys_user/tx3  

 
 ## 🍊技术交流
- QQ交流群： 254350046
- 作者微信： WwdaiPxj
- 关注官方微信公众号，获取更多资讯  

  ![Image text](weixinewm.jpg)