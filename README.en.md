<p align="center">
	<a href="https://gitee.com/wdyun/dynamic-datasource-spring-boot-starter">
	<h1 align="center">Dynamic Datasource</h1></a>
</p>
<p align="center">
	<strong>🍑 A springboot tool that provides dynamic data sources</strong>
</p>
<p align="center">
	👉 <a href="https://gitee.com/wdyun">https://gitee.com/wdyun</a> 👈
</p>

<p align="center">
	<a target="_blank" href="https://search.maven.org/artifact/com.enbatis/dynamic-datasource-spring-boot-starter">
		<img alt="Maven Central" src="https://img.shields.io/maven-central/v/com.enbatis/dynamic-datasource-spring-boot-starter">
	</a>
	<a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0">
		<img src="https://img.shields.io/badge/license-Apache%202.0-green" />
	</a>
	<a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-1.8%2B-red" />
	</a>
	<a href="">
		<img src="https://img.shields.io/badge/maven-v3.6.1-green"/>
	</a>
</p>

<br/>
<p align="center">
	<img src="https://img.shields.io/badge/wechat-wxid__o5c3ce5h7fm822-green"/></a>
</p>

-------------------------------------------------------------------------------

[**🌎Chinese Documentation**](README.md)

-------------------------------------------------------------------------------

## 📚Brief Introduction
dynamic-datasource-spring-boot-starter is a development tool for dynamic switching of multiple data sources based on springboot.

You can easily and quickly develop multi data source projects by importing dependencies in the springboot project, and support distributed transactions of multi data sources.

Escort the development of your multi data source project.


### 🍺Purpose

An open source java project that provides dynamic data source support for the springboot project

Designed to provide one click support for multiple data sources.

### 🐮Characteristic

- The configuration of multiple data sources is simple. You only need to configure additional data sources in the configuration file.
- Support multiple database types (mysql, PostgreSQL, Oracle).
- Supports dynamic switching of data sources using annotations.
- It supports switching multiple data sources from mapper interface layer, which is convenient and fast, and completely solves the trouble of switching data sources from Controller.
- Provide pure read-write separation scheme in mybatis environment.
- Provides JTA atomikos based distributed transactions.

-------------------------------------------------------------------------------

## 📦Install

### 🍊Maven
Add the following in the dependencies of pom.xml of the project:

```xml
<dependency>
  <groupId>com.enbatis</groupId>
  <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
  <version>1.4.1</version>
</dependency>
```
Add the configuration of multiple data sources in the YML file

```yml
spring:
  datasource:
    type: com.alibaba.druid.pool.xa.DruidXADataSource
    url: jdbc:mysql://www.housesale.top:3306/house_sale?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
    username: root
    password: wwd111111
    driver-class-name: com.mysql.cj.jdbc.Driver
    filters: stat,wall
    initialSize: 5
    minIdle: 5
    maxActive: 20
    maxWait: 60000
    timeBetweenEvictionRunsMillis: 60000
    minEvictableIdleTimeMillis: 300000
    validationQuery: SELECT 1 FROM DUAL
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    poolPreparedStatements: true
    maxPoolPreparedStatementPerConnectionSize: 20
    useGlobalDataSourceStat: true
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=500
    dynamic:
      name: master,slave
      default: master
      master:
        type: com.alibaba.druid.pool.xa.DruidXADataSource
        driverClassName: com.mysql.cj.jdbc.Driver
        username: root
        password: wwd111111
        url: jdbc:mysql://www.housesale.top:3306/test01?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
        initialSize: 5
        minIdle: 5
        maxActive: 20
      slave:
        type: com.alibaba.druid.pool.xa.DruidXADataSource
        driverClassName: com.mysql.cj.jdbc.Driver
        username: root
        password: wwd111111
        url: jdbc:mysql://www.housesale.top:3306/test02?useUnicode=true&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC
        initialSize: 5
        minIdle: 5
        maxActive: 20
```
Add the configuration of the data source in the mapper interface
```java

@MybatisMapper(name = "master")
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<SysUser> list1();
}

@MybatisMapper(name = "slave")
public interface SysUserMapper2 extends BaseMapper<SysUser> {

}
```
An example of using distributed transactions is as follows (just add DynamicTx annotation to the methods requiring transactions)
```java
   
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper,SysUser > implements SysUserService {

    @DynamicTx
    @Override
    public boolean tx() {
            SysUser sysUser = new SysUser();
            sysUser.setId(IdWorker.getId());
            sysUser.setSex("男");
            sysUser.setUsername("sysUserMapper1");
            baseMapper.addEntity(sysUser);

            System.out.println(10/0);

            SysUser sysUser2 = new SysUser();
            sysUser2.setId(IdWorker.getId());
            sysUser2.setSex("男");
            sysUser2.setUsername("sysUserMapper2");
            sysUserMapper2.addEntity(sysUser2);
            return false;
    }

}

```
### 🚽Sample project

[https://gitee.com/wdyun/hrm](https://gitee.com/wdyun/hrm) 

```mysql
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(64) NOT NULL COMMENT '主键',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户',
  `sex` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'sex',
  `deleted` int(2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1443577766556602368, 'test01', '男', 0);

SET FOREIGN_KEY_CHECKS = 1;
```



-------------------------------------------------------------------------------