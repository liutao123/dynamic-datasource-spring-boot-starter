package com.enbatis.dynamic.datasource.autoconfigure.dialect.xa;

import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.enbatis.dynamic.datasource.autoconfigure.SqlProperty;
import org.postgresql.xa.PGXADataSource;

import java.sql.SQLException;

/**
 * @author wwd
 */
public class PgsqlXAD {

    public static AtomikosDataSourceBean initPgsqlXA(SqlProperty sqlProperty) {
        PGXADataSource datasource = new PGXADataSource();
        datasource.setUrl(sqlProperty.getUrl());
        datasource.setUser(sqlProperty.getUsername());
        datasource.setPassword(sqlProperty.getPassword());
        //创建atomikos全局事务
        AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
        xaDataSource.setXaDataSource(datasource);
        xaDataSource.setUniqueResourceName(sqlProperty.getName());
        xaDataSource.setXaDataSourceClassName(sqlProperty.getType());

        try {
            xaDataSource.setLoginTimeout(200);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        xaDataSource.setMinPoolSize(Integer.parseInt(sqlProperty.getMinIdle()));
        xaDataSource.setMaxPoolSize(Integer.parseInt(sqlProperty.getMaxActive()));
        xaDataSource.setPoolSize(Integer.parseInt(sqlProperty.getInitialSize()));

        return xaDataSource;
    }

}
