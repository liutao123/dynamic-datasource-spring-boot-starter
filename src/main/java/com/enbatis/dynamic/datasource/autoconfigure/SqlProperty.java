package com.enbatis.dynamic.datasource.autoconfigure;

import com.enbatis.dynamic.datasource.autoconfigure.dynamic.ConstantData;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author wwd
 */
public class SqlProperty {

    private DynamicDatasourceProperties properties = null;

    private String name;

    private String driverClassName;
    private String username;
    private String password;
    private String url;
    private String type;
    private String initialSize;
    private String minIdle;
    private String maxActive;


    private String dataType;

    private String sourceType;

    public SqlProperty(DynamicDatasourceProperties properties, String name) {
        this.properties = properties;
        this.name = name;

        this.driverClassName = properties.getProperty(name + ".driver-class-name");
        this.username = properties.getProperty(name + ".username");
        this.password = properties.getProperty(name + ".password");
        this.url = properties.getProperty(name + ".url");
        this.type = properties.getProperty(name + ".type");
        this.initialSize = properties.getProperty(name + ".initialSize");
        this.minIdle = properties.getProperty(name + ".minIdle");
        this.maxActive = properties.getProperty(name + ".maxActive");

        if (StringUtils.isBlank(driverClassName)) {
            throw new RuntimeException("driverClassName cannot be blank");
        }

        if (StringUtils.isBlank(username)) {
            throw new RuntimeException("username cannot be blank");
        }

        if (StringUtils.isBlank(password)) {
            throw new RuntimeException("password cannot be blank");
        }


        if (StringUtils.isBlank(url)) {
            throw new RuntimeException("url cannot be blank");
        }

        if (driverClassName.contains(ConstantData.MYSQL)) {
            this.dataType = ConstantData.MYSQL;
        } else if (driverClassName.contains(ConstantData.POSTGRESQL)) {
            this.dataType = ConstantData.POSTGRESQL;
        } else if (driverClassName.contains(ConstantData.ORACLE)) {
            this.dataType = ConstantData.ORACLE;
        }

        if (StringUtils.isBlank(type) || type.contains(ConstantData.DS_DRUID)) {
            //default druid
            sourceType = ConstantData.DS_DRUID;
        }
        if (type.contains(ConstantData.DS_DBCP)) {
            //DBCP
            sourceType = ConstantData.DS_DBCP;
        }
        if (type.contains(ConstantData.DS_C3P0)) {
            //C3P0
            sourceType = ConstantData.DS_C3P0;
        }
    }

    public String getName() {
        return name;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public String getInitialSize() {
        return initialSize;
    }

    public String getMinIdle() {
        return minIdle;
    }

    public String getMaxActive() {
        return maxActive;
    }

    public String getDataType() {
        return dataType;
    }

    public String getSourceType() {
        return sourceType;
    }
}
