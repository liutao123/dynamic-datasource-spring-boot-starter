package com.enbatis.dynamic.datasource.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
/**
 * @author wwd
 */
@ConfigurationProperties(prefix = "spring.datasource.dynamic")
public class DynamicDatasourceProperties {

    @Autowired
    private Environment env;


    public String getProperty(String name) {
        return this.env.getProperty("spring.datasource.dynamic." + name);
    }


    public String getDynamicName() {
        return this.env.getProperty("spring.datasource.dynamic.name");
    }

    public String getDefault() {
        return this.env.getProperty("spring.datasource.dynamic.default");
    }


}
