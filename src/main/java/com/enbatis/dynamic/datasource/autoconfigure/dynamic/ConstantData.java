package com.enbatis.dynamic.datasource.autoconfigure.dynamic;

/**
 * @author wwd
 */
public final class ConstantData {
    public final static String MYSQL = "mysql";
    public final static String ORACLE = "oracle";
    public final static String POSTGRESQL = "postgresql";


    public final static String TO_STRING = "toString";


    public final static String DS_DRUID = "DruidDataSource";
    public final static String DS_DBCP = "BasicDataSource";
    public final static String DS_C3P0 = "ComboPooledDataSource";


}
