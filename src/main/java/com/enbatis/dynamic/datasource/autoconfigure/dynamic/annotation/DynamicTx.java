package com.enbatis.dynamic.datasource.autoconfigure.dynamic.annotation;

import java.lang.annotation.*;

/**
 * @author wwd
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DynamicTx {
}
