package com.enbatis.dynamic.datasource.autoconfigure.dynamic.annotation;

import java.lang.annotation.*;

/**
 * @author wwd
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface MybatisMapper {
    String name() default "";
}