package com.enbatis.dynamic.datasource.autoconfigure.dialect;

import com.alibaba.druid.pool.DruidDataSource;
import com.enbatis.dynamic.datasource.autoconfigure.SqlProperty;
import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;

/**
 * @author wwd
 */
public class DruidDS {
    public static DataSource initDruidDs(SqlProperty sqlProperty) {
        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(sqlProperty.getUrl());
        datasource.setUsername(sqlProperty.getUsername());
        datasource.setPassword(sqlProperty.getPassword());
        datasource.setDbType(sqlProperty.getType());
        if (StringUtils.isNotBlank(sqlProperty.getInitialSize())) {
            datasource.setInitialSize(Integer.parseInt(sqlProperty.getInitialSize()));
        }

        if (StringUtils.isNotBlank(sqlProperty.getMinIdle())) {
            datasource.setMinIdle(Integer.parseInt(sqlProperty.getMinIdle()));
        }
        if (StringUtils.isNotBlank(sqlProperty.getMaxActive())) {
            datasource.setMaxActive(Integer.parseInt(sqlProperty.getMaxActive()));
        }
        datasource.setDriverClassName(sqlProperty.getDriverClassName());
        return datasource;
    }

}
