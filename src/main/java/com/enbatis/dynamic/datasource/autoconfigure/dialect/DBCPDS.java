package com.enbatis.dynamic.datasource.autoconfigure.dialect;

import com.alibaba.druid.pool.DruidDataSource;
import com.enbatis.dynamic.datasource.autoconfigure.SqlProperty;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;

/**
 * @author wwd
 */
public class DBCPDS {
    public static DataSource initDbcpDs(SqlProperty sqlProperty) {
        BasicDataSource datasource = new BasicDataSource();
        datasource.setUrl(sqlProperty.getUrl());
        datasource.setUsername(sqlProperty.getUsername());
        datasource.setPassword(sqlProperty.getPassword());
        if (StringUtils.isNotBlank(sqlProperty.getInitialSize())) {
            datasource.setInitialSize(Integer.parseInt(sqlProperty.getInitialSize()));
        }

        if (StringUtils.isNotBlank(sqlProperty.getMinIdle())) {
            datasource.setMinIdle(Integer.parseInt(sqlProperty.getMinIdle()));
        }
        if (StringUtils.isNotBlank(sqlProperty.getMaxActive())) {
            datasource.setMaxIdle(Integer.parseInt(sqlProperty.getMaxActive()));
        }
        datasource.setDriverClassName(sqlProperty.getDriverClassName());
        return datasource;
    }

}
